AlgoInvest&Trade
================

Ce programme permet de maximiser les profits des clients après deux ans d'investissement.

Une fois le programme lancé, vous serez invité à choisir l'option qui vous intéresse, 
deux algorithme sont disponible à l'utilisation.

Contrainte
----------

* Chaque action ne peut être acheté qu'une fois
* Il n'est pas possible d'acheter une fraction d'action
* Le montant maximum pouvant être dépenser par client est de 500€

Tableau d'actions
-----------------

Un fichier nommé "tableau.csv" répertorie les données nécessaires à l'éxecution de l'algorithme.

Ce fichier contient plusieurs informations :

* Une colonne "Actions" représentant les actions pour chaques entreprises.
* Une colonne "Couts" réprésentant la valeur d'**une** action (en €).
* Une colonne "Benefice" réprésentant le bénéfice réalisé après 2 ans d'investissement.

Il y a 2 autres tableaux d'actions contenant beaucoup plus de données, utilisable également dans le 
programme.

# Algorithme

## bruteforce.py

Cette algorithme combinatoire utilise la récursion afin de parcourir toutes les combinaisons possible
à partir de l'investissement indiqué, puis choisi la meilleure combinaison parmis toutes 
celles ayant été générées.

⚠️ **Attention ! Si vous utilisez l'algorithme de bruteforce, il est conseillé de ne l'utiliser qu'avec le tableau d'action 'tableau.csv', dans le cas contraire le temps de résolution pourrait être non polynomial et a terme pourrait faire planter votre machine** ⚠️

## optimized.py

Cette algorithme utilise une méthode gloutonne pour résoudre la meilleure combinaison, celui ci est beaucoup plus performant en terme de rapidité, il n'y a pas de contrainte d'utilisation lié au temps ou performance de votre machine.

# Comparaison des algorithmes

**Complexité temporel & spatiale :**
Si vous souhaitez comparer la compléxité temporel ou spatial des deux algorithmes, le programme propose cette option dans le menu principal, il vous suffit de selectionner celle correspondante afin de lancer une analyse qui génerera un graphique contenant une comparaison entre les deux algorithmes.

# Installation et utilisation

Commencer par clôner le projet :
```
git clone https://gitlab.com/Hekynox/algoinvesttrade.git
```

## Windows

Placer vous dans le répertoire du projet ("algoinvesttrade") puis créer votre environnement virtuel et activez le:
```
python -m venv env
./env/Script/activate
```

Installer les paquets nécessaires : 
```
pip install -r requirements.txt
```

Lancer le programme : 
```
python.exe algoinvest.py
```

## Linux

Placer vous dans le répertoire du projet ("algoinvesttrade") puis créer votre environnement virtuel et activez le:
```
python3 -m venv env
source env/bin/activate
```

Installer les paquets nécessaires : 
```
pip install -r requirements.txt
```

Lancer le programme : 
```
chmod +x ./algoinvest.py
python3 ./algoinvest.py
```