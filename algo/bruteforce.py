import csv
import timeit
import psutil
import os


def get_data(dataset):
    """
    Get data of chart content data needed for establish algorithm.

    Parameters:
    - dataset(str) : path of dataset to use

    Return:
    - data(dict) : data of dataset
    """
    tableau = "../datasets/tableau.csv"
    if dataset == "datasets/tableau.csv" or dataset == tableau:
        chart = dataset

        with open(chart, newline="") as csvfile:
            read_csv = csv.DictReader(csvfile)
            data = {line["Actions"]:
                    (float(line["Couts"]),
                    float(line["Benefice"].rstrip("%"))) for line in read_csv}

    else:
        chart = dataset
        with open(chart, newline="") as csvfile:
            readcsv = csv.DictReader(csvfile)
            data = {line["name"]:
                    (float(line["price"]),
                    float(line["profit"])) for line in readcsv}

    return data


def invest_test(invest, dataset):
    """
    Get data in needed format and launch all possibility search.

    Parameters:
    - invest(int) : amount for buy actions

    Return:
    - Number of combination found
    """
    chart = get_data(dataset)
    # List of tuple content actions with value and profits -> [(str, int, int)]
    cost_benef = [(key, value[0], value[1]) for key, value in chart.items()]
    new_cost_benef = []

    for i in cost_benef:
        if i[1] > 0 and i[2] > 0:
            new_cost_benef.append(i)

    cost_benef_int = [(val1[1], val1[2]) for val1 in new_cost_benef]
    cost_benef_sorted = sorted(cost_benef_int)
    little_nbr = cost_benef_sorted[0][0]
    result = test_possibility(new_cost_benef, invest, little_nbr)
    best_comb = best_combination(result)
    give_comb(best_comb[0], best_comb[1])


def test_possibility(
        cost_benef, invest, little_nbr, current_comb=[], i=0, comb_list=[]
        ):
    """
    determine all combination with recursivity method.

    Parameters:
    - cost_benef(list) : [(price(int), profits%(int), ...] of actions
    - invest(int) : amount for buy actions
    - current_comb(list) : current combination determined
    - i(int) : index position for use on cost_benef(list)
    - comb_list(list) : all combinations that were determined

    Return:
    - comb_list(list) : all combinations that have been determined
    """
    # Get value of actions with current_comb for determine sum of it
    if len(current_comb) > 0:
        current_comb_value = [value[1] for value in current_comb.copy()]
    else:
        current_comb_value = []

    # Check if end of iteration or if current_comb can be additionnal yet
    if i == len(cost_benef) or (sum(current_comb_value) + little_nbr) > invest:
        comb_list.append(current_comb.copy())
        return

    # enter recursivity
    test_possibility(cost_benef, invest, little_nbr, current_comb, i+1,
                     comb_list)

    # Check if sum of current_comb + iteration of cost_benef always
    # lower than invest
    if (sum(current_comb_value) + cost_benef[i][1]) <= invest:
        # Add current iteration in current_comb
        current_comb.append(cost_benef[i])
        # enter recursivity with new current_comb
        test_possibility(cost_benef, invest, little_nbr, current_comb, i+1,
                         comb_list)
        # Delete last iteration of current_comb for tested other combination
        # in its place
        current_comb.pop()

    return comb_list


def best_combination(list_comb):
    """
    Determine which combination of actions can be generated a best profits.

    Parameters:
    - list_comb(list) : [[(str, int, int), (str, int, int)], [(...), (...)]]

    Return:
    - best_comb(list), max_profits(int) : best combination and associated
    profit
    """
    profit_list = []
    best_profit = []
    last_profit = 0

    # iterate on list content all combination
    for i in list_comb:
        profit_list.clear()
        # check if current iterate have something content

        if len(i) > 0:
            for a in i:
                profit = a[1] * a[2] / 100
                profit_list.append(profit)

        profit_total = (sum([a for a in profit_list]))

        if last_profit == 0:
            best_profit = i
            last_profit = profit_total
        else:
            if profit_total > last_profit:
                best_profit = i
                last_profit = profit_total
            else:
                continue

    max_profit = last_profit

    return best_profit, max_profit


def give_comb(best_comb, max_profits):
    """
    Display result of best combination with action can be buy and
    profits effected with this action after 2 years investment.

    Parameters :
    - best_comb(list) : best combination found
    - max_profits(int) : total profit after 2 years investment
    """
    # Get name of Action in a list
    actions = [i[0] for i in best_comb]

    print("Voici les actions à acheter : ")

    for i in actions:
        print(i)

    print(
        f"""Pour un profit total de {max_profits} après 2 ans
        d'investissement""")


def amount_invest():
    """
    Ask to user amount invest.

    No parameters

    Return:
    - amount convert to int
    """
    verify = False

    while verify is False:
        amount = input("Quel montant investir : ")
        if amount.isdigit():
            amount = int(amount)
            if amount > 500 or amount < 4:
                print(f"Vous ne pouvez investir {amount}€")
                print("Le montant investi doit être supérieur à 3€ "
                      "et inférieur à 501€")
            else:
                verify = True
        else:
            print("Merci de rentrer un chiffre/nombre")

    return amount


def main(dataset):
    invest = amount_invest()
    invest_test(invest, dataset)


def data_graph(data, i_action):
    """
    Execute main algorithm to determine time complexity of it.

    Parameters:
    - data(dict) : data of actions array
    - i_action(int) : number of actions to use in a list of data

    Return:
    - time(int) : total time to execute this
    """
    cost_benef = [(key, value[0], value[1]) for key, value in data.items()]
    cost_benef = cost_benef[:i_action]
    cost_benef_int = [(val1[1], val1[2]) for val1 in cost_benef]
    cost_benef_sorted = sorted(cost_benef_int)
    little_nbr = cost_benef_sorted[0][0]
    time_start = timeit.default_timer()
    result = test_possibility(cost_benef, 500, little_nbr)
    best_combination(result)
    time_end = timeit.default_timer()
    time = (time_end - time_start)
    print(f"--- Time execution : {time:.6f} s ---")
    return time


def spatial_graph(data, i_action):
    """
    Execute main algorithm to determine spatial complexity of it.

    Parameters:
    - data(dict) : data of actions array
    - i_action(int) : number of actions to use in a list of data

    Return:
    - time(int) : total time to execute this
    """
    cost_benef = [(key, value[0], value[1]) for key, value in data.items()]
    cost_benef = cost_benef[:i_action]
    cost_benef_int = [(val1[1], val1[2]) for val1 in cost_benef]
    cost_benef_sorted = sorted(cost_benef_int)
    little_nbr = cost_benef_sorted[0][0]
    result = test_possibility(cost_benef, 500, little_nbr)
    best_combination(result)


def launch_spatial(data, i_action):
    mem_analyze = psutil.Process(os.getpid())
    spatial_graph(data, i_action)
    return (mem_analyze.memory_info().rss / 1024 ** 2)


if __name__ == "__main__":
    main()
