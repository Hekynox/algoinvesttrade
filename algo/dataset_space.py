from algo import optimized
from algo import bruteforce
import matplotlib.pyplot as plt
import numpy as np


def main_graph():
    """
    Create a graph comparison of spatial complexity between 'bruteforce.py'
    and 'optimized.py' with dataset 'tableau.csv'.
    """
    dataset = "datasets/tableau.csv"

    dataset_file = optimized.get_data(dataset)

    x_opti = []
    y_opti = []
    x_brute = []
    y_brute = []

    # Loop of get space used needed to create plot for algorithm optimized
    for i in range(len(dataset_file)):
        i_action = i+1
        mem_used_opti = optimized.launch_spatial(dataset_file, i_action)
        x_opti.append(i+1)
        y_opti.append(mem_used_opti)

    # Loop of get space used needed to create plot for algorithm bruteforce
    for i in range(len(dataset_file)):
        i_action = i+1
        mem_used_brute = bruteforce.launch_spatial(dataset_file, i_action)
        x_brute.append(i+1)
        y_brute.append(mem_used_brute)

    # Create array of numpy with list
    x_opti = np.array(x_opti)
    y_opti = np.array(y_opti)
    x_brute = np.array(x_brute)
    y_brute = np.array(y_brute)

    # Plot curve for each algorithm with memory used collected
    plt.plot(x_opti, y_opti, c='red', label='optimized.py')
    plt.plot(x_brute, y_brute, c='blue', label='bruteforce.py')
    plt.xticks(range(1, 21, 1))
    plt.title(
        "Spatial complexity comparison between bruteforce.py & optimized.py")
    plt.xlabel("Number of action")
    plt.ylabel("Memory used (MB)")
    plt.legend()
    plt.show()


if __name__ == "__main__":
    main_graph()
