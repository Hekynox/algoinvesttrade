import csv
import timeit
import psutil
import os


def get_data(dataset):
    """
    Get data of chart content data needed for establish algorithm.

    Parameters:
    - dataset(str) : path of dataset to use

    Return:
    - data(dict) : data of dataset
    """
    tableau = "../datasets/tableau.csv"
    if dataset == "datasets/tableau.csv" or dataset == tableau:
        chart = dataset

        with open(chart, newline="") as csvfile:
            read_csv = csv.DictReader(csvfile)
            data = {line["Actions"]:
                    (float(line["Couts"]),
                    float(line["Benefice"].rstrip("%"))) for line in read_csv}

    else:
        chart = dataset
        with open(chart, newline="") as csvfile:
            readcsv = csv.DictReader(csvfile)
            data = {line["name"]:
                    (float(line["price"]),
                    float(line["profit"])) for line in readcsv}

    return data


def invest_test(invest, dataset):
    """
    Get data in needed format and launch all possibility search.

    Parameters:
    - invest(int) : amount for buy actions
    - dataset(str) : path of dataset to use
    """
    chart = get_data(dataset)
    # List of tuple content actions with value and profits -> [(str, int, int)]
    cost_benef = [(key, value[0], value[1]) for key, value in chart.items()]
    invest = invest * 100
    new_cost = []

    for a in cost_benef:
        if a[1] > 0 and a[2] > 0:
            new_cost.append(a)

    new_cost = [[a[0], int(round(a[1] * 100)), a[2]] for a in new_cost]
    profit = [(a[1] * a[2] / 100) for a in new_cost]
    n = len(new_cost)
    result, list_actions = test_possibility(invest, new_cost, profit, n)
    result = result / 100
    total_cost = (sum([a[1] for a in list_actions]) / 100)
    give_comb(list_actions, result, total_cost)


def test_possibility(invest, cost_benef, profit, n):
    """
    Determine a best investment combination.

    Parameters:
    - invest(int) : amount of investment (convert in cents)
    - cost_benef(list) : content all actions [(A, cost(int), %(float), (...))]
    - profit(list) : content profit of each action
    - n(int) : number of actions

    Returns:
    - max_profit(int) : Total profit of combination investment
    - list_actions(list) : combination of actions for best profit
    """
    K = [[0 for x in range(invest + 1)] for x in range(n + 1)]
    K_action = [[False for x in range(invest + 1)] for x in range(n + 1)]

    for i in range(n + 1):
        for w in range(invest + 1):
            if i == 0 or w == 0:
                K[i][w] = 0
            elif cost_benef[i-1][1] <= w:
                if profit[i-1] + K[i-1][w - cost_benef[i-1][1]] > K[i-1][w]:
                    K[i][w] = profit[i-1] + K[i-1][w - cost_benef[i-1][1]]
                    K_action[i][w] = True
                else:
                    K[i][w] = K[i-1][w]
            else:
                K[i][w] = K[i-1][w]

    n_invest = invest
    list_actions = []

    for i in range(n, 0, -1):
        if K_action[i][n_invest]:
            list_actions.append(cost_benef[i-1])
            n_invest -= cost_benef[i-1][1]

    max_profit = K[n][invest]

    return max_profit, list_actions


def give_comb(best_comb, max_profits, total_cost):
    """
    Display result of best combination with action can be buy and
    profits effected with this action after 2 years investment.

    Parameters :
    - best_comb(list) : best combination found
    - max_profits(int) : total profit after 2 years investment
    """
    # Get name of Action in a list
    actions = [i[0] for i in best_comb]

    print("Voici les actions à acheter : ")

    for i in actions:
        print(i)

    print(
        f"Pour un profit total de {max_profits:.2f}")
    print(f"total cost : {total_cost:.2f}")


def amount_invest():
    """
    Ask to user amount invest.

    No parameters

    Return:
    - amount convert to int
    """
    verify = False

    while verify is False:
        amount = input("Quel montant investir : ")

        if amount.isdigit():
            amount = int(amount)
            if amount > 500 or amount < 4:
                print(f"Vous ne pouvez investir {amount}€")
                print("Le montant investi doit être supérieur à 3€ "
                      "et inférieur à 501€")
            else:
                verify = True
        else:
            print("Merci de rentrer un chiffre/nombre")

    return amount


def main(dataset):
    invest = amount_invest()
    invest_test(invest, dataset)


def data_graph(data, i_action):
    """
    Execute main algorithm to determine time complexity of it.

    Parameters:
    - data(dict) : data of actions array
    - i_action(int) : number of actions to use in a list of data

    Return:
    - time(int) : total time to execute this
    """
    cost_benef = [(key, value[0], value[1]) for key, value in data.items()]
    cost_benef = cost_benef[:i_action]
    new_cost = []

    for a in cost_benef:
        if a[1] > 0 and a[2] > 0:
            new_cost.append(a)

    new_cost = [[a[0], int(round(a[1] * 100)), a[2]] for a in new_cost]
    n = len(new_cost)
    profit = [(a[1] * a[2] / 100) for a in new_cost]
    invest = 500 * 100
    time_start = timeit.default_timer()
    test_possibility(invest, new_cost, profit, n)
    time_end = timeit.default_timer()
    time = (time_end - time_start)
    print(f"--- Time execution : {time:.6f} s ---")
    return time


def spatial_graph(data, i_action):
    """
    Execute main algorithm to determine spatial complexity of it.

    Parameters:
    - data(dict) : data of actions array
    - i_action(int) : number of actions to use in a list of data

    Return:
    - time(int) : total time to execute this
    """
    cost_benef = [(key, value[0], value[1]) for key, value in data.items()]
    cost_benef = cost_benef[:i_action]
    new_cost = []

    for a in cost_benef:
        if a[1] > 0 and a[2] > 0:
            new_cost.append(a)

    new_cost = [[a[0], int(round(a[1] * 100)), a[2]] for a in new_cost]
    n = len(new_cost)
    profit = [(a[1] * a[2] / 100) for a in new_cost]
    invest = 500 * 100
    test_possibility(invest, new_cost, profit, n)


def launch_spatial(data, i_action):
    mem_analyze = psutil.Process(os.getpid())
    spatial_graph(data, i_action)
    return (mem_analyze.memory_info().rss / 1024 ** 2)


if __name__ == "__main__":
    main()
