#!/usr/bin/python3
# coding: utf-8


from algo import optimized, bruteforce, dataset_time, dataset_space


def main_menu():
    """
    display main menu for select which category.

    Return:
    - choice(str) : input of user to choosing option
    """
    verif = False
    while verif is False:
        print("Choisissez l'option qui vous interesse")
        print("1 - Trouver un plan d'investissement")
        print("2 - Graphique de comparaison temporel des algorithmes")
        print("3 - Graphique de comparaison spatiale des algorithmes")
        print("4 - quitter le programme")
        choice = input("Entrer le chiffre correspondant à votre choix : ")
        if choice == "1":
            menu()
        elif choice == "2":
            print("graphique en cours de génération ...")
            dataset_time.main_graph()
        elif choice == "3":
            print("graphique en cours de génération ...")
            dataset_space.main_graph()
        elif choice == "4":
            verif = True
            return choice


def menu():
    """
    display menu for choosing algo and dataset to use.
    """
    verif = False
    while verif is False:
        print("Choisissez le dataset que vous souhaitez utiliser")
        print("1 - tableau.csv")
        print("2 - dataset1_Python+P7.csv")
        print("3 - dataset2_Python+P7.csv")
        print("4 - dataset1_cleaned.csv (version nettoyé du dataset1)")
        print("5 - dataset2_cleaned.csv (version nettoyé du dataset2)")
        print("")
        choice = input("Entrer le chiffre correspondant à votre choix : ")
        if choice == "1":
            dataset = "datasets/tableau.csv"
            verif = True
        elif choice == "2":
            dataset = "datasets/dataset1_Python+P7.csv"
            verif = True
        elif choice == "3":
            dataset = "datasets/dataset2_Python+P7.csv"
            verif = True
        elif choice == "4":
            dataset = "datasets/dataset1_cleaned.csv"
            verif = True
        elif choice == "5":
            dataset = "datasets/dataset2_cleaned.csv"
            verif = True
        else:
            print("merci d'indiquer un chiffre valide")
    algo_choice = algo(dataset)
    if algo_choice == "algo/bruteforce.py":
        bruteforce.main(dataset)
    elif algo_choice == "algo/optimized.py":
        optimized.main(dataset)


def algo(dataset):
    """
    display menu for choice algoithm to use with dataset.

    Parameters:
    - dataset(str) : file path of dataset

    Return:
    - algo(str) : file path of script
    """
    dataset_1 = "datasets/dataset1_Python+P7.csv"
    dataset_2 = "datasets/dataset2_Python+P7.csv"
    verif = False
    while verif is False:
        print("Avec quel algorithme utiliser le dataset ?")
        print("")
        print("1 - bruteforce.py")
        print("2 - optimized.py")
        print("")
        choice = input("Entrer le chiffre correspondant à votre choix :")
        if choice == "1":
            algo = "algo/bruteforce.py"
            if dataset == dataset_1 or dataset == dataset_2:
                verif_warning = False
                print("!!! Attention !!!")
                print("""
                      L'algorithme bruteforce avec dataset1_Python+P7.csv
                      ou dataset2_Python+P7.csv n'est pas recommandé !
                      """)
                print("Celui ci pourrait faire planter votre machine.")
                print("")
                print("Souhaitez vous continuer quand même en sachant cela ?")
                while verif_warning is False:
                    warning_choice = input("'y' = OUI / 'n' = NON : ")
                    if warning_choice == "y":
                        verif = True
                        verif_warning = True
                    elif warning_choice == "n":
                        verif = False
                        verif_warning = True
                    else:
                        print("merci d'entrer 'y' pour OUI ou 'n' pour NON")
            else:
                verif = True
        elif choice == "2":
            algo = "algo/optimized.py"
            verif = True
        else:
            print("merci d'indiquer un chiffre valide")
    return algo


def main():
    choice = main_menu()
    while choice != "4":
        choice = main_menu()


if __name__ == "__main__":
    main()
